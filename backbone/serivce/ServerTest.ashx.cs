﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackBoneTest
{
    /// <summary>
    /// ServerTest 的摘要说明
    /// </summary>
    public class ServerTest : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string name = context.Request.Params["name"];
            string age=context.Request.Params["age"];
            context.Response.ContentType = "text";
            context.Response.Write("{name:\"huang\",age:\"22\"}");

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}