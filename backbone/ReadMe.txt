﻿1.文件夹介绍

    backbone ：里面是从其他demo 拷下来的backbone.js 文件--->> 网站可以正常使用；

	backbone1.0.0和underscore1.5.1：我在backbone网上下下来的js文件-->>页面报错；

	jquery:jquery的js文件；

	Service:存放后台交互文件；

	webPage:存放前台UI页面；

问题介绍：

    1.在backboneTest1.htm中引入的backbone.js在网页运行时就会报错；
	  报错原因：无法获取未定义或 null 引用的属性“each” ；
	  来源：backbone1.0.0/backbone-min.js 文件；

	2.在BackboneView1.htm中Events 绑定的事件不起作用。

	3.在BackBoneRouter.htm中的路由事件访问时触发，当点击连接时不会触发；

	4.在BackBoneModel1.htm中 当Model.save时。服务器端如何获取model的值，并且如何改model 返回值，返回值的格式是什么样的。

